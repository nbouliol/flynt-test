# Flynt technical test

## Running

Run `docker-compose up` to create images and run the containers

Then run `docker-compose exec back yarn migrate:prod` to run database migration

Then browse http://localhost

## Missings

### Back

I did not write any tests, to do so we can use supertest to call endpoints and assert things happened in database

Or export the endpoint into separate file to unit test the endpoint function

I did not create the team edition endpoint, which will be similar to the team creation endpoint

### Front

I did not create the team edition layout

I could have used a store provider (redux) to manage application state
