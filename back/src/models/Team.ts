import {
  Model,
  Table,
  Column,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript'
import { DataType } from 'sequelize-typescript'
import { User } from './User'

@Table({ tableName: 'teams' })
export class Team extends Model {
  @Column
  name!: string

  @ForeignKey(() => User)
  lead_id!: number

  @BelongsTo(() => User, { as: 'lead', foreignKey: 'lead_id' })
  lead!: User

  @Column(DataType.ARRAY(DataType.INTEGER))
  dev_ids!: Array<number>

  devs!: Array<User>

  @ForeignKey(() => User)
  @Column
  intern_id!: number

  @BelongsTo(() => User, { as: 'intern', foreignKey: 'intern_id' })
  intern!: User
}
