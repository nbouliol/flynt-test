import { Model, Table, Column } from 'sequelize-typescript'
import { DataType } from 'sequelize-typescript'

export enum UserRole {
  lead = 'lead',
  developer = 'developer',
  intern = 'intern',
}

@Table({ tableName: 'users' })
export class User extends Model {
  @Column
  firstname!: string

  @Column
  lastname!: string

  @Column
  email!: string

  @Column(DataType.ENUM({ values: Object.keys(UserRole) }))
  role!: UserRole
}
