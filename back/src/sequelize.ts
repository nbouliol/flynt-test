import { Sequelize } from 'sequelize-typescript'
import { Team } from './models/Team'
import { User } from './models/User'

const { DB_HOST, DB_PASSWORD, DB_NAME, DB_USER } = process.env

export const sequelize = new Sequelize({
  dialect: 'postgres',
  host: DB_HOST,
  password: DB_PASSWORD,
  database: DB_NAME,
  username: DB_USER,
  models: [User, Team],
  repositoryMode: true,
})
