import { Router } from 'express'
import { Repository } from 'sequelize-typescript'
import { User, UserRole } from '../models/User'
import { body, validationResult } from 'express-validator'
import { Team } from '../models/Team'

const include = (userRepository: Repository<User>) =>
  ['lead', 'intern'].map((as) => ({
    as,
    model: userRepository,
    attributes: ['firstname', 'lastname', 'email', 'id'],
  }))

export const teamRouter = (
  teamRepository: Repository<Team>,
  userRepository: Repository<User>
) =>
  Router()
    .get('/', async (req, res) => {
      const teams = await teamRepository.findAll({
        include: include(userRepository),
      })

      const enrichedTeams = [...teams]
      for (const team of enrichedTeams) {
        const devs = await userRepository.findAll({
          where: { id: team.dev_ids },
        })

        team.setDataValue('devs', devs)
      }

      return res.json(enrichedTeams)
    })

    .get('/:id', async (req, res) => {
      const team = await teamRepository.findByPk(req.params.id, {
        include: include(userRepository),
      })

      if (!team) {
        return res.status(404).send()
      }

      const devs = await userRepository.findAll({
        where: { id: team.dev_ids },
      })

      team.setDataValue('devs', devs)

      res.json(team)
    })

    .post(
      '/',
      body('name').notEmpty(),
      body('lead_id').isNumeric(),
      body('intern_id').isNumeric().optional(),
      body('dev_ids', 'must be array of different dev ids')
        .isArray({ min: 2 })
        .custom((array: Array<number>) => array.length === new Set(array).size),
      async (req, res) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(400).json({ errors: errors.array() })
        }

        const { dev_ids, lead_id, intern_id } = req.body

        const developers = await userRepository.findAll({
          where: { id: dev_ids, role: UserRole.developer },
        })
        if (developers.length !== dev_ids.length) {
          return res
            .status(400)
            .json({ errors: [{ msg: 'invalid developer id' }] })
        }

        const teamLead = await teamRepository.findOne({
          where: { lead_id },
          include: include(userRepository),
        })
        if (teamLead) {
          return res
            .status(400)
            .json({ errors: [{ msg: 'lead already in a team' }] })
        }

        if (intern_id) {
          const intern = await userRepository.findAll({
            where: { id: intern_id, role: UserRole.intern },
          })

          if (!intern) {
            return res
              .status(400)
              .json({ errors: [{ msg: 'invalid intern id' }] })
          }
        }

        const team = await teamRepository.create(req.body)

        res.json(team)
      }
    )
