import { Router } from 'express'
import { Repository } from 'sequelize-typescript'
import { User, UserRole } from '../models/User'
import { body, validationResult } from 'express-validator'

export const userRouter = (userRepository: Repository<User>) =>
  Router()
    .get('/', async (req, res) => {
      const users = await userRepository.findAll({ order: [['role', 'asc']] })

      return res.json(users)
    })

    .get('/:id', async (req, res) => {
      const user = await userRepository.findByPk(req.params.id)

      if (!user) {
        res.status(404).send()
      }
      res.json(user)
    })

    .post(
      '/',
      body(['firstname', 'lastname']).notEmpty(),
      body('email').isEmail(),
      async (req, res) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(400).json({ errors: errors.array() })
        }

        const user = await userRepository.create(req.body)

        res.json(user)
      }
    )

    .patch(
      '/:id',
      body(['firstname', 'lastname']).notEmpty().optional(),
      body('email').isEmail().optional(),
      body('role').isIn(Object.values(UserRole)).optional(),
      async (req, res) => {
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
          return res.status(400).json({ errors: errors.array() })
        }

        const user: User | null = await userRepository.findByPk(req.params!.id)

        if (!user) {
          return res.status(404).send()
        }

        const { role } = req.body

        if (role) {
          if (
            role !== user.role &&
            ((user.role === UserRole.intern && role !== UserRole.developer) ||
              (user.role === UserRole.developer && role !== UserRole.lead))
          ) {
            return res.status(400).json({ errors: [{ msg: 'invalid role' }] })
          }
        }

        await user.update(req.body, { logging: console.log })

        return res.json(user)
      }
    )
