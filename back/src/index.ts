import express, { Application, json, Request, Response } from 'express'
import { teamRouter } from './endpoints/team'
import { userRouter } from './endpoints/user'
import { Team } from './models/Team'
import { User } from './models/User'
import { sequelize } from './sequelize'
import cors from 'cors'
import morgan from 'morgan'

const userRepository = sequelize.getRepository(User)
const teamRepository = sequelize.getRepository(Team)

const app: Application = express()
app.use(morgan('dev'))
app.use(
  cors({
    methods: ['GET', 'POST', 'PATCH'],
  })
)
app.use(json())

const { PORT } = process.env

app.get('/', (req: Request, res: Response) => {
  res.send('TS App is Running')
})

app.use('/users', userRouter(userRepository))
app.use('/teams', teamRouter(teamRepository, userRepository))

app.listen(PORT, () => {
  console.log(`server is running on PORT ${PORT} !`)
})
