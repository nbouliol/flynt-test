import React from 'react'
import { Routes, Route, Outlet, Link } from 'react-router-dom'
import Team from './pages/Team'
import Teams from './pages/Teams'
import User from './pages/User'
import Users from './pages/Users'
import Error from './components/Error'

const DisplayLink = ({ to, display }: { to: string; display: string }) => {
  return (
    <li className="text-white text-center m-2">
      <Link to={to}>{display}</Link>
    </li>
  )
}

function Layout() {
  return (
    <div className="flex flex-no-wrap">
      <div className="min-h-screen w-64 absolute sm:relative bg-gray-800 shadow md:h-full flex-col justify-between hidden sm:flex">
        <nav>
          <ul>
            <DisplayLink to="/users" display="Users" />
            <DisplayLink to="/teams" display="Teams" />
          </ul>
        </nav>
      </div>

      <div className="container mx-auto py-10 h-64 md:w-4/5 w-11/12 px-6">
        <div className="w-full h-full ">
          <Outlet />
          <Error />
        </div>
      </div>
    </div>
  )
}

function NoMatch() {
  return (
    <div>
      <h2>Nothing to see here!</h2>
      <p>
        <Link to="/users">Go to the home page</Link>
      </p>
    </div>
  )
}

function App() {
  return (
    <>
      <div className="min-h-full">
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route path="users" element={<Users />}>
              <Route path=":id" element={<User />} />
            </Route>
            <Route path="teams" element={<Teams />}>
              <Route path=":id" element={<Team />} />
            </Route>

            <Route path="*" element={<NoMatch />} />
          </Route>
        </Routes>
      </div>
    </>
  )
}

export default App
