import React, { useState } from 'react'
import { useErrorContext } from '../ErrorContext'

function CreateUser({
  closeModal,
  fetchUsers,
}: {
  closeModal: () => void
  fetchUsers: () => Promise<void>
}) {
  const [user, setUser] = useState({
    firstname: '',
    lastname: '',
    email: '',
  })
  const { setError, setMessage } = useErrorContext()

  const onChange = (value: string) => (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setUser((prevState) => {
      return { ...prevState, [value]: e.target.value }
    })
  }

  const createUser = async () => {
    try {
      const res = await fetch(
        `http://${
          process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
        }/users`,
        {
          method: 'POST',
          body: JSON.stringify(user),
          mode: 'cors',
          headers: { 'content-type': 'application/json' },
        }
      )
      await fetchUsers()
      if (!res.ok) {
        const json = await res.json()
        setError(true)
        setMessage(json.errors[0].msg)
      }
    } catch (e) {
      console.error(e)
    }
    closeModal()
  }

  return (
    <div>
      <div className=" mb-2">
        <h2 className="capitalize font-bold underline">firstname</h2>
        <input
          type={'text'}
          value={user.firstname}
          onChange={onChange('firstname')}
          className="block bg-white w-full border border-slate-300 rounded-md p-2 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
        />
      </div>
      <div className=" mb-2">
        <h2 className="capitalize font-bold underline">lastname</h2>
        <input
          type={'text'}
          value={user.lastname}
          onChange={onChange('lastname')}
          className="block bg-white w-full border border-slate-300 rounded-md p-2 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
        />
      </div>
      <div className=" mb-2">
        <h2 className="capitalize font-bold underline">email</h2>
        <input
          type={'text'}
          value={user.email}
          onChange={onChange('email')}
          className="block bg-white w-full border border-slate-300 rounded-md p-2 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
        />
      </div>
      <button
        onClick={() => {
          createUser()
        }}
        className="p-3 rounded bg-green-500"
      >
        Add
      </button>
    </div>
  )
}

export default CreateUser
