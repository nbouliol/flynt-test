import React from 'react'
import { Link } from 'react-router-dom'
import { User } from '../pages/Users'

function TeamUser({ user }: { user: User }) {
  return (
    <div className="m-2 rounded bg-sky-500 border-gray-700 w-2/3 text-center text-white hover:bg-sky-400">
      <Link to={`/users/${user.id}`}>
        <p>
          <span className="capitalize">{user.firstname}</span>{' '}
          <span className="capitalize">{user.lastname}</span>
        </p>
        <p>{user.email}</p>
      </Link>
    </div>
  )
}

export default TeamUser
