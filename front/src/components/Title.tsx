import React from 'react'

function Title({ children }: { children: React.ReactNode }) {
  return <h1 className="text-3xl font-bold underline mb-5">{children}</h1>
}

export default Title
