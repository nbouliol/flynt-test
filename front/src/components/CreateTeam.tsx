import React, { useEffect, useState } from 'react'
import { useErrorContext } from '../ErrorContext'
import { Team } from '../pages/Teams'
import { User } from '../pages/Users'

interface TeamCreation {
  name: string
  lead_id: number
  dev_ids: string[]
  intern_id?: number
}

function CreateTeam({
  fetchTeams,
  closeModal,
  teams,
}: {
  closeModal: () => void
  fetchTeams: () => Promise<void>
  teams: Team[]
}) {
  const [users, setUsers] = useState<User[]>([])

  const [team, setTeam] = useState<Partial<TeamCreation>>({
    name: '',
    dev_ids: [],
    intern_id: -1,
    lead_id: -1,
  })

  const { setError, setMessage } = useErrorContext()

  useEffect(() => {
    fetchUsers()
  }, [])

  const onChange = (value: keyof TeamCreation) => (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    setTeam((prevState) => {
      if (value === 'name') {
        return { ...prevState, [value]: e.target.value }
      }

      if (value === 'dev_ids') {
        return {
          ...prevState,
          [value]: Array.from(
            // @ts-ignore
            e.target.selectedOptions,
            (option: any) => option.value
          ),
        }
      }

      return { ...prevState, [value]: Number(e.target.value) }
    })
  }

  const createTeam = async () => {
    try {
      const res = await fetch(
        `http://${
          process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
        }/teams`,
        {
          method: 'POST',
          body: JSON.stringify({
            ...team,
            dev_ids: team.dev_ids?.map((x) => Number(x)),
          }),
          mode: 'cors',
          headers: { 'content-type': 'application/json' },
        }
      )
      await fetchTeams()
      if (!res.ok) {
        const json = await res.json()
        setError(true)
        setMessage(json.errors[0].msg)
      }
    } catch (e) {
      console.error(e)
    }
    closeModal()
  }

  const fetchUsers = async () => {
    const res = await fetch(
      `http://${
        process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
      }/users`
    )
    const json = await res.json()
    setUsers(json)
  }

  return (
    <div>
      <div className=" mb-2">
        <h2 className="capitalize font-bold underline">Team name</h2>
        <input
          type={'text'}
          value={team.name}
          onChange={onChange('name')}
          className="block bg-white w-full border border-slate-300 rounded-md p-2 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
        />
      </div>
      <div className=" mb-2">
        <h2 className="capitalize font-bold underline">Lead developer</h2>
        <select
          className="bg-white w-full border border-slate-300 rounded-md focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
          onChange={onChange('lead_id')}
          value={team.lead_id}
        >
          <option hidden disabled value={-1}>
            (select an option)
          </option>
          {users
            .filter((u) => u.role === 'lead')
            .filter((u) => !teams.map((x) => x.lead_id).includes(u.id))
            .map((x) => (
              <option value={x.id} key={x.id}>
                {x.firstname} {x.lastname}
              </option>
            ))}
        </select>
      </div>
      <div className=" mb-2">
        <h2 className="capitalize font-bold underline">Developers</h2>
        <select
          multiple
          className="bg-white w-full border border-slate-300 rounded-md focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
          onChange={onChange('dev_ids')}
          value={team.dev_ids}
        >
          {users
            .filter((u) => u.role === 'developer')
            .map((x) => (
              <option value={x.id} key={x.id}>
                {x.firstname} {x.lastname}
              </option>
            ))}
        </select>
      </div>
      <div className=" mb-2">
        <h2 className="capitalize font-bold underline">Intern</h2>
        <select
          className="bg-white w-full border border-slate-300 rounded-md focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
          onChange={onChange('intern_id')}
          value={team.intern_id}
        >
          <option hidden disabled value={-1}>
            (select an option)
          </option>
          {users
            .filter((u) => u.role === 'intern')
            .map((x) => (
              <option value={x.id} key={x.id}>
                {x.firstname} {x.lastname}
              </option>
            ))}
        </select>
      </div>

      <button
        onClick={() => {
          createTeam()
          fetchTeams()
          closeModal()
        }}
        className="p-3 rounded bg-green-500"
      >
        Create
      </button>
    </div>
  )
}

export default CreateTeam
