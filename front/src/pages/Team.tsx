import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import TeamUser from '../components/TeamUser'
import { Team as ITeam } from './Teams'

function Team() {
  let { id } = useParams()

  const [team, setTeam] = useState<ITeam>()

  const fetchTeam = async () => {
    const res = await fetch(
      `http://${
        process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
      }/teams/${id}`
    )
    const json = await res.json()
    setTeam(json)
  }

  useEffect(() => {
    fetchTeam()
  }, [id])

  if (!team) {
    return <p>no team found</p>
  }

  return (
    <div>
      <p className="capitalize text-3xl font-bold mb-5">
        <span className="underline decoration-sky-500">{team.name}</span> team
      </p>

      <h2 className="font-bold">Lead :</h2>
      <TeamUser user={team.lead} />

      <h2 className="font-bold">Developers :</h2>
      {team.devs.map((user) => (
        <TeamUser key={team.id + '_' + user.id} user={user} />
      ))}

      <h2 className="font-bold">Intern :</h2>
      {team.intern ? (
        <TeamUser user={team.intern} />
      ) : (
        <p>This team has no intern</p>
      )}
    </div>
  )
}

export default Team
