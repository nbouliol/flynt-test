import { Dialog } from '@headlessui/react'
import React, { useEffect, useState } from 'react'
import { Link, Outlet } from 'react-router-dom'
import CreateTeam from '../components/CreateTeam'
import Title from '../components/Title'
import { User } from './Users'

export interface Team {
  id: number
  name: string
  lead: User
  intern?: User
  dev_ids: number[]
  lead_id: number
  intern_id?: number
  devs: User[]
}

function Teams() {
  const [teams, setTeams] = useState<Team[]>([])
  let [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    fetchTeams()
  }, [])

  const fetchTeams = async () => {
    const res = await fetch(
      `http://${
        process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
      }/teams`
    )
    const json = await res.json()
    setTeams(json)
  }

  return (
    <div className="flex ">
      <div className="container mx-auto">
        <Dialog
          open={isOpen}
          onClose={() => setIsOpen(false)}
          className="relative z-50"
        >
          <div className="fixed inset-0 bg-black/30" aria-hidden="true" />
          <div className="fixed inset-0 flex items-center justify-center p-4">
            <Dialog.Panel className="w-full max-w-sm rounded bg-white p-4">
              <CreateTeam
                teams={teams}
                fetchTeams={fetchTeams}
                closeModal={() => {
                  setIsOpen(false)
                }}
              />
            </Dialog.Panel>
          </div>
        </Dialog>
        <div className="flex flex-row justify-between">
          <Title>Teams</Title>
          <button
            className="p-3 rounded bg-green-500 mr-2"
            onClick={() => {
              setIsOpen(true)
            }}
          >
            Create team
          </button>
        </div>
        {teams.length > 0 &&
          teams.map((team) => (
            <Link
              to={String(team.id)}
              className="flex flex-col m-2 rounded bg-slate-500 border-gray-700 w-1/2 text-center text-white hover:bg-slate-400"
              key={team.id}
            >
              <p className="capitalize">
                Team name : <span className="capitalize">{team.name}</span>
              </p>
              <p>
                Lead : <span className="capitalize">{team.lead.firstname}</span>{' '}
                <span className="capitalize">{team.lead.lastname}</span>
              </p>
            </Link>
          ))}
      </div>
      <div className="border-l-black border-l-2 " />
      <div className="container mx-auto h-64 md:w-4/5 w-11/12 px-6">
        <div className="w-full h-full ">
          <Outlet />
        </div>
      </div>
    </div>
  )
}

export default Teams
