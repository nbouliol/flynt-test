import { useEffect, useState } from 'react'
import { Link, Outlet } from 'react-router-dom'
import Title from '../components/Title'
import { Dialog } from '@headlessui/react'
import CreateUser from '../components/CreateUser'

export type Role = 'intern' | 'developer' | 'lead'

export interface User {
  firstname: string
  lastname: string
  email: string
  id: number
  role: Role
  createdAt: Date
  updatedAt: Date
}

function Users() {
  const [users, setUsers] = useState<User[]>([])
  let [isOpen, setIsOpen] = useState(false)
  useEffect(() => {
    fetchUsers()
  }, [])

  const fetchUsers = async () => {
    const res = await fetch(
      `http://${
        process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
      }/users`
    )
    const json = await res.json()
    setUsers(json)
  }

  return (
    <div className="flex ">
      <div className="container mx-auto">
        <Dialog
          open={isOpen}
          onClose={() => setIsOpen(false)}
          className="relative z-50"
        >
          <div className="fixed inset-0 bg-black/30" aria-hidden="true" />
          <div className="fixed inset-0 flex items-center justify-center p-4">
            <Dialog.Panel className="w-full max-w-sm rounded bg-white p-4">
              <CreateUser
                fetchUsers={fetchUsers}
                closeModal={() => {
                  setIsOpen(false)
                }}
              />
            </Dialog.Panel>
          </div>
        </Dialog>
        <div className="flex flex-row justify-between">
          <Title>Users</Title>
          <button
            className="p-3 rounded bg-green-500 mr-2"
            onClick={() => {
              setIsOpen(true)
            }}
          >
            Create user
          </button>
        </div>

        {users.length > 0 &&
          users.map((user) => (
            <Link
              to={String(user.id)}
              className="flex flex-row items-center m-2 rounded bg-slate-500 border-gray-700 w-1/2 text-center text-white hover:bg-slate-400"
              key={user.id}
            >
              <span className="text-sky-400 w-2/5 inline-block  uppercase font-bold">
                {user.role}
              </span>
              <div className="flex flex-col">
                <p className="capitalize">
                  {user.firstname} {user.lastname}
                </p>
                <p>{user.email}</p>
              </div>
            </Link>
          ))}
      </div>
      <div className="border-l-black border-l-2 " />
      <div className="container mx-auto h-64 md:w-4/5 w-11/12 px-6">
        <div className="w-full h-full ">
          <Outlet />
        </div>
      </div>
    </div>
  )
}

export default Users
