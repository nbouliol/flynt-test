import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useErrorContext } from '../ErrorContext'

import { Role, User as IUser } from './Users'

const getAvailableRoles = (currentRole: Role) => {
  const roles = ['intern', 'developer', 'lead']

  if (currentRole === 'developer') {
    return roles.slice(1)
  }
  if (currentRole === 'lead') {
    return roles.slice(2)
  }
  return roles
}

const Display = ({
  value,
  title,
  isEditing,
  onChange,
  role,
}: {
  value: string
  title: string
  isEditing: boolean
  onChange: (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void
  role?: Role
}) => {
  return (
    <div className=" mb-2">
      <h2 className="capitalize font-bold underline">{title}</h2>
      {isEditing ? (
        title === 'role' ? (
          <select
            className="bg-white w-1/2 border border-slate-300 rounded-md focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
            onChange={onChange}
            value={value}
          >
            {getAvailableRoles(role!).map((x) => (
              <option key={x}>{x}</option>
            ))}
          </select>
        ) : (
          <input
            type={'text'}
            value={value}
            onChange={onChange}
            className="block bg-white w-1/2 border border-slate-300 rounded-md p-2 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
          />
        )
      ) : (
        <p>{value}</p>
      )}
    </div>
  )
}

function User() {
  let { id } = useParams()

  const [user, setUser] = useState<IUser>()
  const [isEditing, setIsEditing] = useState(false)
  const { setError, setMessage } = useErrorContext()

  const fetchUser = async () => {
    const res = await fetch(
      `http://${
        process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
      }/users/${id}`
    )
    const json = await res.json()
    setUser(json)
  }
  const updateUser = async () => {
    try {
      const res = await fetch(
        `http://${
          process.env.BACK_URL ? process.env.BACK_URL : 'localhost:3001'
        }/users/${id}`,
        {
          method: 'PATCH',
          body: JSON.stringify(user),
          mode: 'cors',
          headers: { 'content-type': 'application/json' },
        }
      )
      const json = await res.json()
      if (res.ok) {
        setUser(json)
      } else {
        setError(true)
        setMessage(json.errors[0].msg)
        await fetchUser()
      }
    } catch (e) {
      console.error(e)
      setError(true)
      setMessage('Fetch error')
      await fetchUser()
    }
  }

  const onChange = (value: string) => (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    setUser((prevState) => {
      if (prevState === undefined) return undefined
      return { ...prevState, [value]: e.target.value }
    })
  }
  useEffect(() => {
    fetchUser()
    setIsEditing(false)
  }, [id])

  if (!user) {
    return null
  }

  return (
    <div>
      <Display
        onChange={onChange('firstname')}
        isEditing={isEditing}
        value={user.firstname}
        title="firstname"
      />
      <Display
        onChange={onChange('lastname')}
        isEditing={isEditing}
        value={user.lastname}
        title="lastname"
      />
      <Display
        onChange={onChange('email')}
        isEditing={isEditing}
        value={user.email}
        title="email"
      />
      <Display
        onChange={onChange('role')}
        isEditing={isEditing}
        value={user.role}
        title="role"
        role={user.role}
      />
      <div className="flex flex-row">
        {!isEditing && (
          <button
            className="p-3 rounded bg-indigo-500"
            onClick={() => {
              setIsEditing(true)
            }}
          >
            Edit
          </button>
        )}

        {isEditing && (
          <div className="">
            <button
              className="p-3 rounded bg-green-500"
              onClick={() => {
                updateUser()
                setIsEditing(false)
              }}
            >
              save
            </button>
            <button
              className="p-3 rounded bg-red-500"
              onClick={() => {
                setIsEditing(false)
                fetchUser()
              }}
            >
              cancel
            </button>
          </div>
        )}
      </div>
    </div>
  )
}

export default User
