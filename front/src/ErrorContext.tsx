import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react'

export interface IErrorContext {
  error: boolean
  setError: React.Dispatch<React.SetStateAction<boolean>>
  message: string
  setMessage: React.Dispatch<React.SetStateAction<string>>
}

export const ErrorContext = createContext<IErrorContext | null>(null)

export function useErrorContext() {
  const context = useContext(ErrorContext)
  if (context === null) {
    throw new Error(
      'useErrorContext must be used within a ErrorContextProvider'
    )
  }
  return context
}

export const ErrorContextProvider = ({ children }: { children: ReactNode }) => {
  const [message, setMessage] = useState<string>('')
  const [error, setError] = useState(false)

  const value = { message, setMessage, error, setError }
  return <ErrorContext.Provider value={value}>{children}</ErrorContext.Provider>
}
